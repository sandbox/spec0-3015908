<?php

namespace Drupal\qiskit\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures forms module settings.
 */
class QiskitConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'qiskit_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'qiskit.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('qiskit.settings');
    $form['api_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Quantum experience API token'),
      '#default_value' => $config->get('api_token'),
      '#description' => $this->t('You can obtain the token from !url', [
        '!url' => 'https://quantumexperience.ng.bluemix.net/qx/account'
      ]),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('qiskit.settings')
      ->set('api_token', $values)
      ->save();

    parent::submitForm($form, $form_state);
  }

}
