<?php

namespace Drupal\qiskit;

/**
 * Provides an interface defining IBM Quantum Experience.
 */
interface QuantumExperienceInterface {

  /**
   * IBM backends.
   */
  const NAMES_BACKEND_IBMQXV2 = [
    'ibmqx5qv2',
    'ibmqx2',
    'qx5qv2',
    'qx5q',
    'real'
  ];

  const NAMES_BACKEND_IBMQXV3 = ['ibmqx3'];

  const NAMES_BACKEND_SIMULATOR = [
    'simulator',
    'sim_trivial_2',
    'ibmqx_qasm_simulator',
    'ibmq_qasm_simulator'
  ];

  /**
   *
   */
  public function checkBackends($backend, $endpoint);

  /**
   *
   */
  public function checkCredentials($config);

  /**
   *
   */
  public function getExecution($id_execution, $access_token, $user_id);

  /**
   *
   */
  public function getResultFromExecution($id_execution, $access_token, $user_id);

  /**
   *
   */
  public function runExperiment($qasm, $backend, $shots, $name);

  /**
   *
   */
  public function backendStatus($backend, $access_token, $user_id);

  /**
   *
   */
  public function backendCalibration($backend, $hub, $access_token, $user_id);

  /**
   *
   */
  public function backendParameters():

  /**
   *
   */
  public function availableBackends();

}
